import * as actionTypes from "./actionTypes";

export const addProduct = payload => {
	return { type: actionTypes.ADD_PRODUCT, payload };
};

export const removeProduct = payload => {
	return { type: actionTypes.REMOVE_PRODUCT, payload };
};

export const updateProduct = payload => {
	return {type: actionTypes.UPDATE_PRODUCT, payload}
};