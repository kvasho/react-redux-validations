import React, { Component } from "react";
import { connect }          from "react-redux";
import PropTypes            from "prop-types";

//Styles
import "./styles.scss";

//Actions
import * as actions from "./actions";

//Components
import UserInfo from "../../components/UserInfo";


class StepOne extends Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let isValid = 0;
		return (
			<div>
				<h1>Enter your Contact Data</h1>
				<form action = "">
					{
						this.props.user.map(item => {

							if (item.errorMessage.length > 0 || item.value.length < 1) {
								isValid++;
							}

							return (
								<UserInfo
									key = { item.id }
									id = { item.id }
									value = { item.value }
									type = { item.type }
									placeholder = { item.placeholder }
									validRules = { item.validRules }
									errorMessage = { item.errorMessage }
									onChangeHandler = { (value, id, validRules) => this.onChangeHandler(value, id, validRules) }
									onBlurHandler = { (value, id, validRules) => this.onBlurHandler(value, id, validRules) }
								/>
							);
						})
					}
				</form>

				<p>
					<button onClick = { this.props.nextStep } className={ !isValid ? "active" : "not-active"}>Next Step</button>
				</p>
			</div>
		);
	}

	onBlurHandler = (value, id, validRules) => {
		let errorMessage = null;
		if (value === "") {
			errorMessage = "Enter Information";
			this.props.updateUser({ value, id, validRules });
		}
		return errorMessage;
	};

	onChangeHandler = (value, id, validRules) => {
		this.props.updateUser({ value, id, validRules });
	};
}

StepOne.propTypes = {
	updateUser: PropTypes.func,
};

const mapStateToProps = state => {

	return {
		user: state.user.info
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateUser:         payload => dispatch(actions.updateUser(payload)),
		updateErrorMessage: payload => dispatch(actions.updateErrorMessage(payload))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StepOne);