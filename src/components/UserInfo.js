import React        from "react";
import PropTypes    from "prop-types";
import ReactTooltip from "react-tooltip";

//Styles
import "./styles.scss";

import * as type from "../common/constants/inputTypes";

const UserInfo = React.memo(props => {

	const { errorMessage, validRules, id, placeholder,value } = props;
	let inputElement = null;
	switch (props.type) {
		case (type.text):
			inputElement = (
				<React.Fragment>
					<ReactTooltip
						type = "error"
						effect = "solid"
						place = "left"
						globalEventOff='click'
					/>

					<input
						data-event='mouseenter'
						data-event-off='click'
						data-tip = { errorMessage }
						onBlur = { (event) => props.onBlurHandler(event.target.value, id, validRules) }
						onChange = { (event) => props.onChangeHandler(event.target.value, id, validRules) }
						type = { type.text }
						placeholder = { placeholder }
						value = { value }
					/>
				</React.Fragment>

			);
			break;
		case (type.number) :
			inputElement = <React.Fragment>
				<ReactTooltip
					type = "error"
					effect = "solid"
					place = "left"
					globalEventOff='click'
				/>
				<input
					data-event='mouseenter'
					data-event-off='click'
					data-tip = { errorMessage }
					onBlur = { (event) => props.onBlurHandler(event.target.value, id, validRules) }
					onChange = { (event) => props.onChangeHandler(event.target.value, id, validRules) }
					type = { type.number }
					placeholder = { placeholder }
					value = { value }
				/>
			</React.Fragment>;
			break;
		case (type.textArea) :
			inputElement =
				<React.Fragment>
					<ReactTooltip
						type = "error"
						effect = "solid"
						place = "left"
						globalEventOff='click'
					/>
					<textarea
						data-event='mouseenter'
						data-event-off='click'
						data-tip = { props.errorMessage }
						onBlur = { (event) => props.onBlurHandler(event.target.value, props.id, props.validRules) }
						onChange = { (event) => props.onChangeHandler(event.target.value, props.id, props.validRules) }
						placeholder = { props.placeholder }
						value = { props.value }
					/>
				</React.Fragment>;
			break;
		case (type.email) :
			inputElement =
				<React.Fragment>
					<ReactTooltip
						type = "error"
						effect = "solid"
						place = "left"
						globalEventOff='click'
					/>
					<input
						onBlur = { (event) => props.onBlurHandler(event.target.value, props.id, props.validRules) }
						onChange = { (event) => props.onChangeHandler(event.target.value, props.id, props.validRules) }
						type = { type.email }
						placeholder = { props.placeholder }
						value = { props.value }
					/>
				</React.Fragment>;
			break;
		case (type.checkbox) :
			inputElement = (
				<label>
					<input
						onChange = { () => props.onChangeHandler(!props.value, props.id, props.validRules) }
						type = { type.checkbox }
						checked = { props.value }
					/>
					{ props.placeholder }
				</label>
			);
			break;
		default :
			inputElement = null;
			break;
	}

	return (
		<div style = { { marginTop: "20px" } }>
			{ inputElement }
		</div>
	);
});

UserInfo.propTypes = {
	errorMessage: PropTypes.string,
	placeholder: PropTypes.string,


};

export default UserInfo;