import { combineReducers } from "redux";

import user from "../containers/StepOne/reducer";
import Product from "../containers/StepTwo/reducer";

const rootReducer = combineReducers({
	user,
	Product
});

export default rootReducer;