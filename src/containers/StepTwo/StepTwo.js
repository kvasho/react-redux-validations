import React, { Component } from "react";
import { connect }          from "react-redux";

//Components
import Products from "../../components/Products/Products";

//Styles
import "./styles.scss";

//Functions
import * as val     from "../../common/helpers/checkValid";
import * as actions from "../StepTwo/actions";


class StepTwo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id:               null,
			increment:        0,
			prodName:         "",
			price:            "",
			amount:           "",
			errorMessageNum:  "",
			errorMessageName: ""
		};

	}

	render() {
		const { prodName, price, amount } = this.state;
		return (
			<div>
				<h1>Add Product Here</h1>
				<div>
					<input
						type = "text"
						onChange = { (e) => this.onNameChange(e) }
						value = { prodName }
						placeholder = "Product Name"
					/>
					<input
						type = "number"
						onChange = { (e) => this.onPriceChange(e) }
						value = { price }
						placeholder = "Price"
					/>
					<input
						type = "number"
						onChange = { (e) => this.onAmountChange(e) }
						value = { amount }
						placeholder = "Amount"
					/>
					<button
						className = "add-btn"
						onClick = { () => this.addProduct(prodName, price, amount) }
					>Add Product
					</button>
				</div>
				{
					this.props.items.map(item => {
						return (
							<Products
								key = { item.id }
								id = { item.id }
								name = { item.prodName }
								price = { item.price }
								amount = { item.amount }
								onDelete = { (id) => this.removeProduct(id) }
								onEdit = { (id, prodName, price, amount) => this.editProduct(id, prodName, price, amount) }
								onUpdate = { (id, prodName, price, amount) => this.updateProduct(id, prodName, price, amount) }
							/>
						);
					})
				}
				<p>
					<button
						className = "prev-step"
						onClick = { this.props.previousStep }
					>Previous Step
					</button>
				</p>
			</div>
		);
	}

	onNameChange = e => {
		this.setState({
			prodName: e.target.value
		});
	};

	onPriceChange = e => {
		this.setState({
			price: e.target.value
		});
	};

	onAmountChange = e => {
		this.setState({
			amount: e.target.value
		});
	};

	addProduct = (prodName, price, amount) => {
		const id = this.state.increment;
		val.checkProdVal();
		this.props.addProduct({ id, prodName, price, amount });

		this.setState({
			increment: this.state.increment + 1,
			prodName: "",
			price:    "",
			amount:   ""
		});
	};


	removeProduct = (id) => {
		this.props.removeProduct({ id });

	};

	editProduct = (id, prodName, price, amount) => {
		this.setState({
			id:       id,
			prodName: prodName,
			price:    price,
			amount:   amount,
		});
	};

	updateProduct = () => {
		const { id, prodName, price, amount } = this.state;
		this.props.updateProduct({ id, prodName, price, amount });
	};
}

const mapStateToProps = state => {
	return {
		items: state.Product.product
	};

};

const mapDispatchToProps = dispatch => {
	return {
		addProduct:    payload => dispatch(actions.addProduct(payload)),
		removeProduct: payload => dispatch(actions.removeProduct(payload)),
		updateProduct: payload => dispatch(actions.updateProduct(payload))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StepTwo);