import * as actionTypes from "./actionTypes";

export const updateUser = payload => {
	return { type: actionTypes.UPDATE_USER, payload };
};

export const updateErrorMessage = payload => {
	return {type: actionTypes.UPDATE_ERROR_MESSAGE,payload};
};