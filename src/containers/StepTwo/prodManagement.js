export const addProduct = (state, payload) => {
	return {
		...state,
		product: [...state.product,payload]
	};
};

export const removeProduct = (state, payload) => {

	const index = state.product.findIndex(item => item.id === payload.id);
	return {
		...state,
		product: [...state.product.slice(0,index), ...state.product.slice(index + 1)]
	};

};

export const updateProduct = (state, payload) => {
	return {
		...state,
		product: [
			...state.product.map(item => {
				if (item.id === payload.id) {
					return {
						...payload
					}
				}
				return { ...item }
			})
		]
	}
};

