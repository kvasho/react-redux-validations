import * as rule from "../constants/validationRules";

export const checkValidity = (value, rules) => {
	let isValid = "";
	switch (rules) {
		case rule.required:
			if (value.length < 1) {
				isValid = "Input field is required";
			}
			break;
		case rule.underEighteen:
			if (value < 18) {
				isValid = "Under 18 age is not required";
			}
			break;
		case rule.isEmail:
			const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
			isValid       = pattern.test(value)
							? ""
							: "Enter Valid Email";
			break;
		default:
	}

	return isValid;
};

export const checkProdVal = (value, rules) => {
	let isValid = "";

	switch (rules) {
		case rule.required:
			if (value.length < 1) {
				isValid = "Input field is required";
			}
			break;
		case rule.greaterZero:
			if (value < 0) {
				isValid = "Number must be greater than zero";
			}
			return isValid;
		default:
	}
};