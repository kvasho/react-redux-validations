import * as validRules from "../../common/helpers/checkValid";

export const updateUser = (state, payload) => {

	const errorMessage = validRules.checkValidity(payload.value,payload.validRules);

	return {
		...state,
		info:        [
			...state.info.map(item => {
				if (item.id === payload.id) {
					return {
						...item,
						value: payload.value,
						errorMessage
					};
				}

				return { ...item };
			})
		]
	};
};

