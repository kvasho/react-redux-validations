import React,{Component}       from "react";
import { connect } from "react-redux";
import StepWizard from 'react-step-wizard';

//Containers
import StepOne from "./containers/StepOne/StepOne";
import StepTwo from "./containers/StepTwo/StepTwo";

import "./App.css";

class App extends Component {
	render() {
		return (
			<div className = "App">
				<StepWizard
					isHashEnabled={true}
					initalStep={1}
				>
					<StepOne
						hashKey={'StepOne'}
					/>
					<StepTwo
						hashKey={'StepTwo'}
					/>
				</StepWizard>
			</div>
		);
	}
}

const mapStateToProps = state => {

	return {
		posts: state.test
	};
};


export default connect(mapStateToProps)(App);
