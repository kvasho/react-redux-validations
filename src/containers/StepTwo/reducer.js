//Action types
import * as actionTypes from "./actionTypes";

//User management functions
import * as prodMan from "./prodManagement";

const initState = {
	product: []
};

export default function(state = initState, action) {
	switch (action.type) {
		case actionTypes.ADD_PRODUCT:
			return prodMan.addProduct(state, action.payload);
		case actionTypes.REMOVE_PRODUCT:
			return prodMan.removeProduct(state, action.payload);
		case actionTypes.UPDATE_PRODUCT:
			return prodMan.updateProduct(state, action.payload);
		default:
			return { ...state }
	}
};