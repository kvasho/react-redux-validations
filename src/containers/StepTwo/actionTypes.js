export const ADD_PRODUCT          = "ADD_PRODUCT";
export const REMOVE_PRODUCT       = "REMOVE_PRODUCT";
export const UPDATE_ERROR_MESSAGE = "UPDATE_ERROR_MESSAGE";
export const UPDATE_PRODUCT       = "UPDATE_PRODUCT";