export const nameId           = "name";
export const surNameId        = "surName";
export const ageId            = "age";
export const emailId          = "email";
export const marriedId        = "married";
export const unemployedId     = "unemployed";
export const aboutYourselfId  = "aboutYourself";
export const workExperienceId = "workExperience";
