import * as con  from "../../common/constants/validationRules";
import * as type from "../../common/constants/inputTypes";
import * as ID   from "../../common/constants/infoIdes";

//Action types
import * as actionTypes from "./actionTypes";

//User management functions
import * as userMan from "./userManagement";

const initState = {
	info: [
		{
			id:          ID.nameId,
			value:       "",
			type:        type.text,
			placeholder: "Name",
			validRules:  con.required,
			errorMessage: ""
		},
		{
			id:          ID.surNameId,
			value:       "",
			type:        type.text,
			placeholder: "Last name",
			validRules:  con.required,
			errorMessage: ""
		},
		{
			id:          ID.ageId,
			value:       "",
			type:        type.number,
			placeholder: "Age",
			validRules:  con.underEighteen,
			errorMessage: ""
		},
		{
			id:          ID.emailId,
			value:       "",
			type:        type.email,
			placeholder: "Email",
			validRules:  con.isEmail,
			errorMessage: ""
		},
		{
			id:          ID.marriedId,
			value:       false,
			type:        type.checkbox,
			placeholder: "Married",
			validRules:  null,
			errorMessage: ""
		},
		{
			id:          ID.unemployedId,
			value:       false,
			type:        type.checkbox,
			placeholder: "Unemployed",
			validRules:  null,
			errorMessage: ""
		},
		{
			id:          ID.aboutYourselfId,
			value:       "",
			type:        type.textArea,
			placeholder: "About Yourself",
			validRules:  con.required,
			errorMessage: ""
		},
		{
			id:          ID.workExperienceId,
			value:       "",
			type:        type.textArea,
			placeholder: "Work Experience",
			validRules:  con.required,
			errorMessage: ""
		},
	],
	isInfoValid: null
};

export default function(state = initState, action) {
	switch (action.type) {
		case actionTypes.UPDATE_USER:
			return userMan.updateUser(state, action.payload);
		default:
			return { ...state }
	}
};