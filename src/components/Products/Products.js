import React, { Component } from "react";

//Style
import "./products.scss";

class Products extends Component {
	render() {
		const { name, price, amount, id,onUpdate,onEdit,onDelete } = this.props;
		return (
			<div className = "product-form">

				<div className = "products">
					<h1>{ name }</h1>
					<h1>{ price }</h1>
					<h1>{ amount }</h1>
					<button
						className = "delete-btn"
						onClick = { () => onDelete (id) }
					>Delete
					</button>
					<button
						className = "edit-btn"
						onClick = { () => onEdit(id, name, price, amount) }
					>Edit
					</button>
					<button onClick = { () => onUpdate(id, name, price, amount) }>Update</button>
				</div>


			</div>
		);
	}
}

export default Products;